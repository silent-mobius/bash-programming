# Bash Programming



.footer: Created By Alex M. Schapelle, VAioLabs.io


---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is shell programming ?
- Who needs it ?
- How shell programmiung works ?
- Do's and don'ts of shell programming.
- Basic concepts of Shell programming.
- Looping and Branching.
- Stuctures, functions and libraries.
- Automation with shell programming.


### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of shell
- For junior/senior developers who are still developing on LAMP/LEMP/WAMP/MAMP stacks and do automation.
- Experienced ops who need refresher


---

# Course Topics

- Intro
- Basics
- Built-ins
- Operators
- Scripting intro
- Parameters
- Conditions
- Loops
- Arrays
- Functions
- Libs
- Automation

---
# About Me
<img src="../99_misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        - Yes, one of them was A+.
        - Yes, one of them was Cisco.
        - Yes, one of them was RedHat course.
        - Yes, one of them was LPIC1 and Shell scripting.
        - No, others i learned alone.
        - No, not maintaining debian packages any more.
---

# About Me (cont.)
- over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - Js admirer
    - Golang fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

You can find me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

---
# History

A shell script is a computer program designed to be run by the Unix shell, a command-line interpreter.[1] The various dialects of shell scripts are considered to be scripting languages. Typical operations performed by shell scripts include file manipulation, program execution, and printing text. A script which sets up the environment, runs the program, and does any necessary cleanup, logging, etc. is called a wrapper.

---
# History (cont.)

The term is also used more generally to mean the automated mode of running an operating system shell; in specific operating systems they are called other things such as batch files (MSDos-Win95 stream, OS/2), command procedures (VMS), and shell scripts (Windows NT stream and third-party derivatives like 4NT—article is at cmd.exe), and mainframe operating systems are associated with a number of terms.

---
# History (cont.)

The typical Unix/Linux/POSIX-compliant installation includes the KornShell (ksh) in several possible versions such as ksh88, Korn Shell '93 and others. The oldest shell still in common use is the Bourne shell (sh); Unix systems may also include the C shell (csh), Bash (bash), a Remote Shell (rsh), a Secure Shell (ssh) for SSL telnet connections, and a shell which is a main component of the Tcl/Tk installation usually called tclsh; wish is a GUI-based Tcl/Tk shell. The C and Tcl shells have syntax quite similar to that of said programming languages, and the Korn shells and Bash are developments of the Bourne shell, which is based on the ALGOL language with elements of a number of others added as well. On the other hand, the various shells plus tools like awk, sed, grep, and BASIC, Lisp, C and so forth contributed to the Perl programming language.

